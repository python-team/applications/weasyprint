Source: weasyprint
Section: text
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Scott Kitterman <scott@kitterman.com>
Build-Depends: debhelper-compat (= 13),
               dh-python (>= 5.20211213),
               flit (>= 3.2),
               libglib2.0-0t64,
               libpango-1.0-0,
               libpangoft2-1.0-0,
               pybuild-plugin-pyproject,
               python3,
               python3-cffi,
               python3-cssselect2,
               python3-fonttools,
               python3-html5lib,
               python3-pil,
               python3-pydyf,
               python3-pyphen,
               python3-sphinx,
Standards-Version: 4.7.0
Homepage: https://weasyprint.org/
Vcs-Browser: https://salsa.debian.org/python-team/packages/weasyprint
Vcs-Git: https://salsa.debian.org/python-team/packages/weasyprint.git
Rules-Requires-Root: no

Package: weasyprint
Architecture: all
Depends: libpango-1.0-0,
         libpangoft2-1.0-0,
         shared-mime-info,
         python3-cffi,
         ${misc:Depends},
         ${python3:Depends}
Recommends: ca-certificates
Description: Document factory for creating PDF files from HTML
 WeasyPrint is a smart solution helping web developers to create PDF
 documents. It turns simple HTML pages into gorgeous statistical reports,
 invoices, tickets, etc.
 .
 From a technical point of view, WeasyPrint is a visual rendering engine for
 HTML and CSS that can export to PDF and PNG. It aims to support web standards
 for printing. WeasyPrint is free software made available under a BSD license.
 .
 It is based on various libraries but *not* on a full rendering engine like
 WebKit or Gecko. The CSS layout engine is written in Python, designed for
 pagination, and meant to be easy to hack on.
